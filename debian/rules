#!/usr/bin/make -f

# use local fork of debcargo and dh-cargo
PATH := $(CURDIR)/debian/dh-cargo/bin:$(PATH)
PERL5LIB = $(CURDIR)/debian/dh-cargo/lib
export PATH PERL5LIB

# resolve all direct entries in first "package..." section in TODO
VENDORLIBS = $(shell perl -nE \
 '/^(?:\s{2}[*]\s*(?:(package)|(\S+))|\s{4}[*]\s*(\S+))/ or next; \
 $$i++ if $$1; exit if $$i > 1 or $$2; say $$3 if $$3 and $$i;' \
 debian/TODO)

# generate cargo-checksum file
_mkchecksum = printf '{"package":"%s","files":{}}\n' \
 $$(sha256sum $(or $2,$(dir $1)Cargo.toml) | grep -Po '^\S+') > $1;

# Avoid tests requiring non-free subtitle files
TEST_NONFREE = \
 idx::parse_index \
 probe::probe_idx_files \
 probe::probe_sub_files \
 sub::parse_fuzz_corpus_seeds \
 sub::parse_subtitles \
 sub::parse_subtitles_from_subtitle_edit \
 src/lib.rs

%:
	dh $@ --buildsystem cargo --sourcedirectory vobsub

# FIXME: stop tolerate failures when dh-cargo-built-using succeeds
override_dh_auto_test:
	dh_auto_test -- --no-fail-fast -- $(addprefix --skip ,$(TEST_NONFREE)) || true

# custom target unused during official build
get-vendorlibs:
# preserve needed crates
	cargo vendor --manifest-path vobsub/Cargo.toml --versioned-dirs debian/vendorlibs~
	rm -rf debian/vendorlibs
	mkdir debian/vendorlibs
	cd debian/vendorlibs~ && mv --target-directory=../vendorlibs $(VENDORLIBS)

# tolerate binary files in preserved code
	find debian/vendorlibs -type f ! -size 0 | perl -lne 'print if -B' \
		> debian/source/include-binaries

	rm -f vobsub/Cargo.lock

	$(eval GONE = $(filter-out 0,\
		$(shell grep -Fxc '      * package is missing' debian/TODO)))
	$(eval INCOMPLETE = $(filter-out 0,\
		$(shell grep -Fc '      * package lacks feature' debian/TODO)))
	$(eval BROKEN = $(filter-out 0,\
		$(shell grep -c '      \* package is .*broken' debian/TODO)))
	$(eval OLD = $(filter-out 0,\
		$(shell grep -Fxc '      * package is outdated' debian/TODO)))
	$(eval AHEAD = $(filter-out 0,\
		$(shell grep -Fxc '      * package is ahead' debian/TODO)))
	$(eval SNAPSHOT = $(filter-out 0,\
		$(shell grep -Fxc '      * package should cover snapshot' debian/TODO)))
	$(eval REASONS = $(strip\
		$(if $(GONE),$(GONE)_missing)\
		$(if $(BROKEN),$(BROKEN)_broken)\
		$(if $(INCOMPLETE),$(INCOMPLETE)_incomplete)\
		$(if $(OLD),$(OLD)_outdated)\
		$(if $(AHEAD),$(AHEAD)_ahead)\
		$(if $(SNAPSHOT),$(SNAPSHOT)_unreleased)))
	$(eval c = ,)
	@echo
	@echo "DONE: embedding $$(ls -1 debian/vendorlibs | grep -c .) crates\
	 $(if $(REASONS),($(subst _,$() ,$(subst $() ,$c_,$(REASONS)))))"
	@echo
